/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "masters/inputmaster.h"
#include "controllable.h"

Controllable::Controllable(Context* context): SceneObject(context),
    controlled_{ false },
    move_{},
    aim_{},

    actions_{},
    actionSince_{},
    actionInterval_{}
{
    SetUpdateEventMask(USE_UPDATE | USE_FIXEDUPDATE);

    for (int a{ 0 }; a < 4; ++a)
    {
        actionSince_[a] = actionInterval_[a] = 1.0f;
    }
}

void Controllable::Start()
{
    SceneObject::Start();

    rigidBody_ = node_->CreateComponent<RigidBody2D>();
    rigidBody_->SetBodyType(BT_DYNAMIC);
    rigidBody_->SetFixedRotation(true);

    collisionShape_ = node_->CreateComponent<CollisionCircle2D>();
    collisionShape_->SetRadius(1.f);
    collisionShape_->SetRestitution(2/3.f);

    graphicsNode_ = node_->CreateChild();
    graphicsNode_->Rotate({ 90.f, Vector3::LEFT });
}

void Controllable::Update(float timeStep)
{
    for (int a{ 0 }; a < static_cast<int>(actions_.size()); ++a)
    {
        if (actions_[a] || actionSince_[a] > 0.0f)
            actionSince_[a] += timeStep;
    }

    if (GetPlayer() && GetPlayer()->IsHuman())
    {

    }
    else
    {
        Think(timeStep);
    }
}

void Controllable::SetMove(Vector2 move)
{
    move = move.Normalized() * Pow(move.Length() * 1.05f, 2.0f);

    if (move.Length() > 1.0f)
        move.Normalize();

    move_ = move;
}

void Controllable::SetAim(Vector2 aim)
{
    aim_ = aim.Normalized();
}

void Controllable::SetActions(std::bitset<4> actions)
{
    if (actions == actions_)
    {
        return;
    }
    else
    {
        for (int i{ 0 }; i < static_cast<int>(actions.size()); ++i)
        {

            if (actions[i] != actions_[i])
            {
                actions_[i] = actions[i];

                if (actions[i] && actionSince_[i] > actionInterval_[i])
                    HandleAction(i);
            }
        }
    }
}

void Controllable::HandleAction(int actionId)
{
    actionSince_[actionId] = 0.0f;
}

void Controllable::AlignWithMovement(float timeStep)
{
    Quaternion rot{ node_->GetRotation() };
    Quaternion targetRot{};
    targetRot.FromLookRotation(move_);
    rot = rot.Slerp(targetRot, Clamp(timeStep * 23.0f, 0.0f, 1.0f));
    node_->SetRotation(rot);
}

void Controllable::AlignWithVelocity(float timeStep)
{
    if (rigidBody_->GetLinearVelocity().LengthSquared() > 0.01f)
    {
        Vector2 v{ rigidBody_->GetLinearVelocity().Normalized() };
        graphicsNode_->SetRotation(Quaternion{ v.Angle(Vector2::UP), Vector3{ v }.CrossProduct(Vector3::DOWN).NormalizedOrDefault(Vector3::FORWARD) }
                                   * Quaternion{ 90.f, Vector3::LEFT });
    }
}

void Controllable::ClearControl()
{
    ResetInput();
}

Player* Controllable::GetPlayer()
{
    return GetSubsystem<InputMaster>()->GetPlayerByControllable(this);
}

