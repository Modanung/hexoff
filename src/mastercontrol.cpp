/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "masters/inputmaster.h"
#include "masters/spawnmaster.h"
#include "player/player.h"
#include "player/ship.h"
#include "pickups/heart.h"
#include "pickups/apple.h"
#include "enemies/razor.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl* MasterControl::instance_ = NULL;

MasterControl* MasterControl::GetInstance()
{
    return MasterControl::instance_;
}

MasterControl::MasterControl(Context *context):
    Application(context)
{
    instance_ = this;
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "heXoff.log";
    engineParameters_[EP_WINDOW_TITLE] = "heXoff";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_FULL_SCREEN] = false;
    engineParameters_[EP_BORDERLESS] = true;
//    engineParameters_[EP_WINDOW_WIDTH] = 640;
//    engineParameters_[EP_WINDOW_HEIGHT] = 480;
    engineParameters_[EP_RESOURCE_PATHS] = "Data;CoreData;Resources;";
}
void MasterControl::Start()
{
    RegisterSubsystem<InputMaster>();
    RegisterSubsystem<SpawnMaster>();

    context_->RegisterFactory<Ship>();
    context_->RegisterFactory<Bullets>();
    context_->RegisterFactory<Razor>();
    context_->RegisterFactory<Pickup>();
    context_->RegisterFactory<Heart>();
    context_->RegisterFactory<Apple>();

//    RENDERER->SetMaterialQuality(QUALITY_LOW);
//        ENGINE->SetMaxFps(60);

    CreateScene();
}
void MasterControl::Stop()
{
    engine_->DumpResources(true);
}
void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();
    PhysicsWorld2D* physics{ scene_->CreateComponent<PhysicsWorld2D>() };
    physics->SetGravity(Vector2::ZERO);
    physics->SetPositionIterations(2);
    physics->SetVelocityIterations(3);
    scene_->CreateComponent<DebugRenderer>();

    RENDERER->GetDefaultZone()->SetAmbientColor(Color::WHITE * (1.f/3.f));

    //Light
    Node* lightNode{ scene_->CreateChild("Light") };
    lightNode->SetPosition({ 2.f, -5.f, -23.f });
    lightNode->LookAt(Vector3::ZERO);
    Light* light{ lightNode->CreateComponent<Light>() };
    light->SetLightType(LIGHT_DIRECTIONAL);
    light->SetBrightness(3.4f);
    light->SetRange(100.f);
    light->SetPerVertex(true);

    //Camera
    Node* cameraNode{ scene_->CreateChild("Camera") };
    cameraNode->SetPosition(Vector3{ 0.f, -23.f, -42.f });
    cameraNode->SetRotation(Quaternion{ -25.f, 0.f, 0.f });

    Camera* camera{ cameraNode->CreateComponent<Camera>() };
    camera->SetNearClip(1.f);
    camera->SetFarClip(100.f);

    float viewScale{ 1.f };
    IntRect viewRect{
                RoundToInt(.5f * GRAPHICS->GetWidth()  * (1.f - viewScale)),
                RoundToInt(.5f * GRAPHICS->GetHeight() * (1.f - viewScale)),
                RoundToInt((.5f * viewScale + .5f) * GRAPHICS->GetWidth() ),
                RoundToInt((.5f * viewScale + .5f) * GRAPHICS->GetHeight()) };
    RENDERER->SetViewport(0, new Viewport{ context_, scene_, camera, viewRect });

    //Ship
    shipNode_ = scene_->CreateChild("Ship");
    Ship* ship{ shipNode_->CreateComponent<Ship>() };
    ship->Set(Vector3::ZERO);

    GetSubsystem<InputMaster>()->SetPlayerControl(AddPlayer(), ship);


    //Enemies
    for (int i{ 0 }; i < 5; ++i)
    {
        SPAWN->Create<Razor>()->Set(Vector3::RIGHT * (3.f + 2.f * i) + Vector3::UP * 5.f * (i % 2));
    }

    //Pickups
    SPAWN->Create<Heart>(true);
    SPAWN->Create<Apple>(true);
}

Player* MasterControl::AddPlayer()
{
    players_.Push(SharedPtr<Player>{ new Player{ players_.Size() + 1, context_ } });

    return players_.Back();
}

Vector<SharedPtr<Player> > MasterControl::GetPlayers()
{
    return players_;
}

Player* MasterControl::GetPlayer(int playerId) const
{
    for (Player* p: players_)
    {
        if (p->GetPlayerId() == playerId)
            return p;
    }

    return nullptr;
}

Player* MasterControl::GetNearestPlayer(const Vector3 pos) const
{
    Player* nearest{};
    for (Player* p : players_)
    {
        if (p->IsAlive())
        {
            if (!nearest
                || (LucKey::Distance(GetSubsystem<InputMaster>()->GetControllableByPlayer(p)->GetWorldPosition(), pos) <
                    LucKey::Distance(GetSubsystem<InputMaster>()->GetControllableByPlayer(nearest)->GetWorldPosition(), pos)))
            {
                nearest = p;
            }
        }
    }

    return nearest;
}

