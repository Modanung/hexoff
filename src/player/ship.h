/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SHIP_H
#define SHIP_H

#include "../projectiles/bullets.h"

#include "../controllable.h"
#include "../destructable.h"

class Ship: public Controllable, public Destructable
{
    DRY_OBJECT(Ship, Controllable);

public:
    Ship(Context* context);

    void Start() override;
    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;

    Vector<Ship*> All() { return ships_; }
    int ColorSet() const { return colorSet_; }

protected:
    void Destroy() override;

private:
    void Shoot();
    void FireBullet(const Vector2 direction);

    void DrawDebug(StringHash eventType, VariantMap& eventData);

    Bullets* bullets_;
    static Vector<Ship*> ships_;
    int colorSet_;

    int weaponLevel_;
    int bulletAmount_;
    const float initialShotInterval_;
    float shotInterval_;
    float sinceLastShot_;

    int appleCount_;
    int heartCount_;
};

#endif // SHIP_H
