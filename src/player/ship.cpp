/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "ship.h"

Vector<Ship*> Ship::ships_{};

Ship::Ship(Context* context): Controllable(context), Destructable(10.f),
    weaponLevel_{ 0 },
    bulletAmount_{ 1 },
    initialShotInterval_{ .30f },
    shotInterval_{ initialShotInterval_ },
    sinceLastShot_{ 0.f },
    appleCount_{ 0 },
    heartCount_{ 0 }
{
    thrust_ = 2.3e4;
    maxSpeed_ = 45.f;

    ships_.Push(this);
    colorSet_ = ships_.Size();
}

void Ship::Start()
{
    Controllable::Start();

    StaticModel* shipModel{ graphicsNode_->CreateComponent<StaticModel>() };
    shipModel->SetModel(RES(Model, "Models/KlåMk10.mdl"));
    shipModel->SetMaterial(0, RES(Material, "Materials/Green.xml"));
    shipModel->SetMaterial(1, RES(Material, "Materials/GreenGlow.xml"));

    rigidBody_->SetLinearDamping(.5f);
    collisionShape_->SetDensity(1.f / M_PI);

    bullets_ = GetScene()->CreateComponent<Bullets>();
    bullets_->Init(colorSet_);

    SubscribeToEvent(E_POSTUPDATE, DRY_HANDLER(Ship, DrawDebug));
}

void Ship::Update(float timeStep)
{
    Controllable::Update(timeStep);

    AlignWithVelocity(timeStep);

    //Shooting
    sinceLastShot_ += timeStep;

    if (aim_.Length() > M_EPSILON
     && sinceLastShot_ > shotInterval_)
        Shoot();
}

void Ship::Shoot()
{
    Player* player{ GetPlayer() };
    const int maxBullets{ Clamp(player->GetScore(),
                                1u,
                                static_cast<unsigned>(bulletAmount_)) };

    for (int i{ 0 }; i < maxBullets; ++i)
    {
        float angle{ 0.0f };
        switch (i)
        {
        case 0: angle = (maxBullets == 2 || maxBullets == 3) ? -5.0f
                                                             :  0.0f;
            break;
        case 1: angle = maxBullets < 4 ? 5.0f
                                       : 7.5f;
            break;
        case 2: angle = maxBullets < 5 ? 180.0f
                                       : 175.0f;
            break;
        case 3: angle = -7.5f;
            break;
        case 4: angle = 185.0f;
            break;
        default: break;
        }
        const Vector2 direction{ LucKey::Rotate(aim_.Normalized(), angle) };
        FireBullet(direction);
    }

    if (player->GetScore() == 0)
    {
        sinceLastShot_ = -.666f;
    }
    else
    {
        player->Pay(maxBullets);
        sinceLastShot_ = 0.f;
    }

    //Create a single muzzle flash
//    if (bulletAmount_ > 0)
//    {
//        MoveMuzzle();

//        SoundEffect* shotSound{ SPAWN->Create<SoundEffect>() };
//        shotSound->Set(node_->GetWorldPosition());
//        shotSound->PlaySample(MC->GetSample("Shot"), 0.1f);
//        PlaySample(MC->GetSample("Shot_s"), 0.14f, false);
//    }
}

void Ship::FireBullet(const Vector2 direction)
{
    const float damage{ 0.1f + .0023f * weaponLevel_ };

    bullets_->Add(node_->GetPosition2D(),
                  direction * 3.f * (23.0f + 0.42f * weaponLevel_),
                  damage);
}

void Ship::FixedUpdate(float timeStep)
{
    if (move_.LengthSquared() == 0.0f)
        return;

    const float m{ rigidBody_->GetMass() };
    const float v{ rigidBody_->GetLinearVelocity().Length() };
    const float d{ rigidBody_->GetLinearVelocity().Normalized().DotProduct(move_.Normalized()) };
    const float maxThrust{ 2.f * m * (maxSpeed_ - v * d) / timeStep };
    const float thrust{ Min(maxThrust, thrust_ * move_.LengthSquared() ) };

    Vector2 force{ move_.Normalized() * thrust * timeStep };
    rigidBody_->ApplyForceToCenter(force, true);
}

void Ship::Destroy()
{
    Disable();
}

void Ship::DrawDebug(StringHash eventType, VariantMap& eventData)
{
//    GetScene()->GetComponent<PhysicsWorld2D>()->DrawDebugGeometry();

    DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };
    debug->AddCross(node_->GetWorldPosition() + Vector3{ aim_ }, 1.f, Color::WHITE);
    debug->AddCircle(Vector3::ZERO, Vector3::BACK, 24.0f, Color::CYAN, 6);
}
