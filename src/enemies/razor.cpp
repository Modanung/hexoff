/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "razor.h"

StaticModelGroup* Razor::razorGroup_{ nullptr };
StaticModelGroup* Razor::bladeGroup_{ nullptr };
StaticModelGroup* Razor::thornGroup_{ nullptr };

Razor::Razor(Context* context): Enemy(context, 1.f)
{
}

void Razor::Start()
{
    Controllable::Start();
    SetMove(Vector2{ Random(-1.f, 1.f), Random(-1.f, 1.f) }.NormalizedOrDefault(Vector2::UP));

    if (!razorGroup_)
    {
        razorGroup_ = GetScene()->CreateComponent<StaticModelGroup>();
        razorGroup_->SetModel(RES(Model, "Models/Razor.mdl"));
        razorGroup_->SetMaterial(RES(Material, "Materials/Black.xml"));
    }

    rose_ = Random(2) == 1;

    if (!rose_)
    {
        if (!bladeGroup_)
        {
            bladeGroup_ = GetScene()->CreateComponent<StaticModelGroup>();
            bladeGroup_->SetModel(RES(Model, "Models/Blade.mdl"));
            bladeGroup_->SetMaterial(RES(Material, "Materials/Cyans.xml"));
        }

    }
    else
    {
        if (!thornGroup_)
        {
            thornGroup_ = GetScene()->CreateComponent<StaticModelGroup>();
            thornGroup_->SetModel(RES(Model, "Models/Blade.mdl"));
            thornGroup_->SetMaterial(RES(Material, "Materials/Magentas.xml"));
        }
    }

    top_ = graphicsNode_->CreateChild("Top");
    razorGroup_->AddInstanceNode(top_);

    for (int i{ 0 }; i < 3; ++i)
    {
        Node* blade{ top_->CreateChild("Blade") };
        blade->SetPosition(Vector3{ 0.f, .22f, .95f });
        blade->RotateAround(Vector3::ZERO, Quaternion(120.f * i, Vector3::UP), TS_PARENT);
        if (!rose_)
            bladeGroup_->AddInstanceNode(blade);
        else
            thornGroup_->AddInstanceNode(blade);

        blades_.Push(blade);
    }

    rigidBody_->SetLinearDamping(.5f);
    collisionShape_->SetDensity(2.f / M_PI);
}

void Razor::Update(float timeStep)
{
    top_->Yaw((1.f + 3.f * Panic()) * 235.f * timeStep);
}

void Razor::FixedUpdate(float timeStep)
{
    const Vector2 v{ rigidBody_->GetLinearVelocity() };
    const float s{ v.Length() };

    if (s > 0.f)
        SetMove(v);

    const float maxSpeed{ (1.f + (2.f + !rose_) * Panic()) * 10.f/3.f };

    Vector2 force{ move_.Normalized() * (maxSpeed - s) * 1.e3 * timeStep };
    rigidBody_->ApplyForceToCenter(force, true);
}

void Razor::Hit(float damage, int pId)
{
    Destructable::Hit(damage, pId);

    for (Node* n: blades_)
        n->Yaw(damage * (1.f + rose_) * 42.f);

    const float mass{ rigidBody_->GetMass() };
    collisionShape_->SetRadius(1.f + Panic() * (1.f + rose_) * .25f);
    rigidBody_->SetMass(mass);
}
