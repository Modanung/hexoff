/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef RAZOR_H
#define RAZOR_H

#include "enemy.h"

class Razor: public Enemy
{
    DRY_OBJECT(Razor, Enemy);

public:
    Razor(Context* context);

    void Start() override;

    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;

    void Hit(float damage, int pId);

    Node* top_;
    Node* bottom_;

private:
    bool rose_;

    static StaticModelGroup* razorGroup_;
    static StaticModelGroup* bladeGroup_;
    static StaticModelGroup* thornGroup_;

    Vector<Node*> blades_;
};

#endif // RAZOR_H
