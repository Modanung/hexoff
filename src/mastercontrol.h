/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

#define ARENA_SIZE 23
#define ARENA_RADIUS Cos(30.f) * (ARENA_SIZE + 1.0f)

namespace Dry {
class Node;
class Scene;
}

class Player;
class Heart;
class Apple;

class MasterControl : public Application
{
    DRY_OBJECT(MasterControl, Application);
public:
    MasterControl(Context* context);
    static MasterControl* GetInstance();

    Scene* GetScene() const { return scene_; }

    Player* AddPlayer();
    Player* GetPlayer(int playerId) const;
    Player* GetNearestPlayer(const Vector3 pos) const;
    Vector< SharedPtr<Player> > GetPlayers();
    void RemovePlayer(Player *player);

    // Setup before engine initialization. Modifies the engine paramaters.
    virtual void Setup();
    // Setup after engine initialization.
    virtual void Start();
    // Cleanup after the main loop. Called by Application.
    virtual void Stop();
    void Exit();

    template <class T> void RegisterObject() { context_->RegisterFactory<T>(); }
    template <class T> void RegisterSubsystem() { context_->RegisterSubsystem(new T(context_)); }

    static Vector2 GetHexant(const Vector3& position)
    {
        Vector2 flatPos{ VectorTo2D(position) };

        const int sides{ 6 };
        Vector2 hexant{ Vector2::UP };

        for (int h{ 0 }; h < sides; ++h)
        {
            Vector2 otherHexantNormal{ LucKey::Rotate(Vector2::UP, h * (360.0f / sides)) };
            hexant = flatPos.Angle(otherHexantNormal) < flatPos.Angle(hexant) ? otherHexantNormal : hexant;
        }

        return hexant;
    }

    static bool InsideHexagon(const Vector3& position, float radius)
    {
        const Vector2 flatPosition{ VectorTo2D(position) };
        const Vector2 hexant{ GetHexant(position) };
        const float boundsCheck{ flatPosition.Length() * LucKey::Cosine(M_DEGTORAD * flatPosition.Angle(hexant)) };

        if (boundsCheck > radius + M_EPSILON)
            return false;
        else
            return true;
    }

    float SinePhase(float freq, float shift)
    {
        return M_TAU * (freq * scene_->GetElapsedTime() + shift);
    }
    float Sine(const float freq, const float min, const float max, const float shift)
    {
        float phase{ SinePhase(freq, shift) };
        float add{ 0.5f * (min + max) };

        return Sin(ToDegrees(phase)) * 0.5f * (max - min) + add;
    }

private:
    void CreateScene();

    Node* shipNode_;

    static MasterControl* instance_;
    Scene* scene_;
    Vector< SharedPtr<Player> > players_;

    Heart* heart_;
    Apple* apple_;
};

#endif // MASTERCONTROL_H

