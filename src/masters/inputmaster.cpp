/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../player/player.h"

#include "inputmaster.h"

using namespace LucKey;

InputMaster::InputMaster(Context* context): Object(context)
{
    keyBindingsMaster_[KEY_UP]     = buttonBindingsMaster_[static_cast<int>(SixaxisButton::SB_DPAD_UP)]    = MasterInputAction::UP;
    keyBindingsMaster_[KEY_DOWN]   = buttonBindingsMaster_[static_cast<int>(SixaxisButton::SB_DPAD_DOWN)]  = MasterInputAction::DOWN;
    keyBindingsMaster_[KEY_LEFT]   = buttonBindingsMaster_[static_cast<int>(SixaxisButton::SB_DPAD_LEFT)]  = MasterInputAction::LEFT;
    keyBindingsMaster_[KEY_RIGHT]  = buttonBindingsMaster_[static_cast<int>(SixaxisButton::SB_DPAD_RIGHT)] = MasterInputAction::RIGHT;
    keyBindingsMaster_[KEY_RETURN] = buttonBindingsMaster_[static_cast<int>(SixaxisButton::SB_CROSS)]      = MasterInputAction::CONFIRM;
    keyBindingsMaster_[KEY_ESCAPE] = buttonBindingsMaster_[static_cast<int>(SixaxisButton::SB_CIRCLE)]     = MasterInputAction::CANCEL;
    keyBindingsMaster_[KEY_P]      = buttonBindingsMaster_[static_cast<int>(SixaxisButton::SB_START)]      = MasterInputAction::PAUSE;
    keyBindingsMaster_[KEY_ESCAPE] = MasterInputAction::MENU;

    keyBindingsPlayer_[1][KEY_W]      = PlayerInputAction::MOVE_UP;
    keyBindingsPlayer_[1][KEY_S]      = PlayerInputAction::MOVE_DOWN;
    keyBindingsPlayer_[1][KEY_A]      = PlayerInputAction::MOVE_LEFT;
    keyBindingsPlayer_[1][KEY_D]      = PlayerInputAction::MOVE_RIGHT;
    keyBindingsPlayer_[1][KEY_C]      = PlayerInputAction::DASH;
    keyBindingsPlayer_[1][KEY_V]      = PlayerInputAction::FIRE;
    keyBindingsPlayer_[1][KEY_KP_8]   = PlayerInputAction::FIRE_N;
    keyBindingsPlayer_[1][KEY_KP_5]   = PlayerInputAction::FIRE_S;
    keyBindingsPlayer_[1][KEY_KP_2]   = PlayerInputAction::FIRE_S;
    keyBindingsPlayer_[1][KEY_KP_4]   = PlayerInputAction::FIRE_W;
    keyBindingsPlayer_[1][KEY_KP_6]   = PlayerInputAction::FIRE_E;
    keyBindingsPlayer_[1][KEY_KP_9]   = PlayerInputAction::FIRE_NE;
    keyBindingsPlayer_[1][KEY_KP_3]   = PlayerInputAction::FIRE_SE;
    keyBindingsPlayer_[1][KEY_KP_1]   = PlayerInputAction::FIRE_SW;
    keyBindingsPlayer_[1][KEY_KP_7]   = PlayerInputAction::FIRE_NW;
    keyBindingsPlayer_[1][KEY_8]   = PlayerInputAction::FIRE_N;
    keyBindingsPlayer_[1][KEY_I]   = PlayerInputAction::FIRE_S;
    keyBindingsPlayer_[1][KEY_K]   = PlayerInputAction::FIRE_S;
    keyBindingsPlayer_[1][KEY_U]   = PlayerInputAction::FIRE_W;
    keyBindingsPlayer_[1][KEY_O]   = PlayerInputAction::FIRE_E;
    keyBindingsPlayer_[1][KEY_9]   = PlayerInputAction::FIRE_NE;
    keyBindingsPlayer_[1][KEY_L]   = PlayerInputAction::FIRE_SE;
    keyBindingsPlayer_[1][KEY_J]   = PlayerInputAction::FIRE_SW;
    keyBindingsPlayer_[1][KEY_7]   = PlayerInputAction::FIRE_NW;

    keyBindingsPlayer_[2][KEY_UP]     = PlayerInputAction::MOVE_UP;
    keyBindingsPlayer_[2][KEY_DOWN]   = PlayerInputAction::MOVE_DOWN;
    keyBindingsPlayer_[2][KEY_LEFT]   = PlayerInputAction::MOVE_LEFT;
    keyBindingsPlayer_[2][KEY_RIGHT]  = PlayerInputAction::MOVE_RIGHT;
    keyBindingsPlayer_[2][KEY_LSHIFT] = PlayerInputAction::DASH;

    SubscribeToEvent(E_KEYDOWN, DRY_HANDLER(InputMaster, HandleKeyDown));
    SubscribeToEvent(E_KEYUP, DRY_HANDLER(InputMaster, HandleKeyUp));
    SubscribeToEvent(E_JOYSTICKBUTTONDOWN, DRY_HANDLER(InputMaster, HandleJoystickButtonDown));
    SubscribeToEvent(E_JOYSTICKBUTTONUP, DRY_HANDLER(InputMaster, HandleJoystickButtonUp));
    SubscribeToEvent(E_JOYSTICKAXISMOVE, DRY_HANDLER(InputMaster, HandleJoystickAxisMove));
    SubscribeToEvent(E_UPDATE, DRY_HANDLER(InputMaster, HandleUpdate));
}

void InputMaster::HandleUpdate(StringHash /*eventType*/, VariantMap &/*eventData*/)
{
    InputActions activeActions{};
    for (Player* p: MC->GetPlayers())
    {
        int pId{ p->GetPlayerId() };
        Vector<PlayerInputAction> emptyActions{};
        activeActions.player_[pId] = emptyActions;
    }

    //Convert key presses to actions
    for (int key: pressedKeys_)
    {
        //Check for master key presses
        if (keyBindingsMaster_.Contains(key))
        {
            MasterInputAction action{keyBindingsMaster_[key]};
            if (!activeActions.master_.Contains(action))
                activeActions.master_.Push(action);
        }
        //Check for player key presses
        for (Player* p: MC->GetPlayers())
        {
            int pId{ p->GetPlayerId() };
            if (keyBindingsPlayer_[pId].Contains(key)){
                PlayerInputAction action{keyBindingsPlayer_[pId][key]};
                if (!activeActions.player_[pId].Contains(action))
                    activeActions.player_[pId].Push(action);
            }
        }
    }
    //Check for joystick button presses
    for (Player* p: MC->GetPlayers())
    {
        int pId{ p->GetPlayerId() };
        for (int button: pressedJoystickButtons_[pId-1])
        {
            if (buttonBindingsPlayer_[pId].Contains(button))
            {
                PlayerInputAction action{ buttonBindingsPlayer_[pId][button]};

                if (!activeActions.player_[pId].Contains(action))
                    activeActions.player_[pId].Push(action);
            }
        }
    }

    //Handle the registered actions
    HandleActions(activeActions);
}

void InputMaster::HandleActions(const InputActions& actions)
{
    //Handle master actions
    for (MasterInputAction action: actions.master_)
    {
        switch (action){
        case MasterInputAction::UP:                 break;
        case MasterInputAction::DOWN:               break;
        case MasterInputAction::LEFT:               break;
        case MasterInputAction::RIGHT:              break;
        case MasterInputAction::CONFIRM:            break;
        case MasterInputAction::CANCEL:             break;
        case MasterInputAction::PAUSE:              break;
        case MasterInputAction::MENU: MC->Exit();   break;
        default: break;
        }
    }

    //Handle player actions
    for (Player* p: MC->GetPlayers())
    {
        int pId{ p->GetPlayerId() };
        auto playerInputActions = actions.player_[pId];
        Controllable* controlled{ controlledByPlayer_[pId] };

        if (controlled)
        {
            Vector2 stickMove{ axesPosition_[pId-1][0], -axesPosition_[pId-1][1] };
            Vector2 stickAim { axesPosition_[pId-1][2], -axesPosition_[pId-1][3] };

            controlled->SetMove(GetMoveFromActions(playerInputActions) + stickMove);
            controlled->SetAim(GetAimFromActions(playerInputActions) + stickAim);

            std::bitset<4>restActions{};
            restActions[0] = playerInputActions->Contains(PlayerInputAction::DASH);
            restActions[1] = playerInputActions->Contains(PlayerInputAction::FIRE);

            controlled->SetActions(restActions);
        }
    }
}

void InputMaster::Screenshot()
{
    Image screenshot{ context_ };
    Graphics* graphics{ GetSubsystem<Graphics>() };
    graphics->TakeScreenShot(screenshot);
    //Here we save in the Data folder with date and time appended
    String fileName{ GetSubsystem<FileSystem>()->GetProgramDir() + "Screenshots/Screenshot_" +
                Time::GetTimeStamp().Replaced(':', '_').Replaced('.', '_').Replaced(' ', '_')+".png" };
    //Log::Write(1, fileName);
    screenshot.SavePNG(fileName);
}

void InputMaster::HandleKeyDown(StringHash eventType, VariantMap &eventData)
{
    int key{ eventData[KeyDown::P_KEY].GetInt() };

    if (!pressedKeys_.Contains(key))
        pressedKeys_.Push(key);
}

void InputMaster::HandleKeyUp(StringHash /*eventType*/, VariantMap &eventData)
{
    int key{ eventData[KeyUp::P_KEY].GetInt() };

    if (pressedKeys_.Contains(key))
        pressedKeys_.Remove(key);
}

void InputMaster::HandleJoystickButtonDown(StringHash /*eventType*/, VariantMap &eventData)
{
    int joystickId{ eventData[JoystickButtonDown::P_JOYSTICKID].GetInt() };
    SixaxisButton button{ static_cast<SixaxisButton>(eventData[JoystickButtonDown::P_BUTTON].GetInt()) };

    if (!pressedJoystickButtons_[joystickId].Contains(button))
        pressedJoystickButtons_[joystickId].Push(button);
}
void InputMaster::HandleJoystickButtonUp(StringHash /*eventType*/, VariantMap &eventData)
{
    int joystickId{ eventData[JoystickButtonDown::P_JOYSTICKID].GetInt() };
    SixaxisButton button{ static_cast<SixaxisButton>(eventData[JoystickButtonUp::P_BUTTON].GetInt()) };

    if (pressedJoystickButtons_[joystickId].Contains(button))
        pressedJoystickButtons_[joystickId].Remove(button);
}

void InputMaster::HandleJoystickAxisMove(Dry::StringHash /*eventType*/, Dry::VariantMap& eventData)
{
    int joystickId{ eventData[JoystickAxisMove::P_JOYSTICKID].GetInt() };
    int axis{ eventData[JoystickAxisMove::P_AXIS].GetInt() };
    float position{ eventData[JoystickAxisMove::P_POSITION].GetFloat() };

    axesPosition_[joystickId][axis] = position;
}

Vector2 InputMaster::GetMoveFromActions(Vector<PlayerInputAction>* actions)
{
    return Vector2::RIGHT * (actions->Contains(PlayerInputAction::MOVE_RIGHT) -
                             actions->Contains(PlayerInputAction::MOVE_LEFT))
         + Vector2::UP    * (actions->Contains(PlayerInputAction::MOVE_UP) -
                             actions->Contains(PlayerInputAction::MOVE_DOWN));
}
Vector2 InputMaster::GetAimFromActions(Vector<PlayerInputAction>* actions)
{
    return Vector2::RIGHT * (actions->Contains(PlayerInputAction::FIRE_E) -
                             actions->Contains(PlayerInputAction::FIRE_W))
         + Vector2::UP    * (actions->Contains(PlayerInputAction::FIRE_N) -
                             actions->Contains(PlayerInputAction::FIRE_S))

         + Vector2{ 1.f, -1.f }.Normalized() *
                 (actions->Contains(PlayerInputAction::FIRE_SE) -
                  actions->Contains(PlayerInputAction::FIRE_NW))
         + Vector2{ 1.f, 1.f }.Normalized() *
                 (actions->Contains(PlayerInputAction::FIRE_NE) -
                  actions->Contains(PlayerInputAction::FIRE_SW));
}

void InputMaster::SetPlayerControl(Player* player, Controllable* controllable)
{
    int playerId{ player->GetPlayerId() };

    if (controlledByPlayer_.Contains(playerId))
    {
        if (controlledByPlayer_[playerId] == controllable)
            return;

        controlledByPlayer_[playerId]->ClearControl();
    }

    controlledByPlayer_[playerId] = controllable;
}

Player* InputMaster::GetPlayerByControllable(Controllable* controllable)
{
    for (int k: controlledByPlayer_.Keys())
    {
        if (controlledByPlayer_[k] == controllable)
            return MC->GetPlayer(k);
    }

    return nullptr;
}

Controllable* InputMaster::GetControllableByPlayer(Player* player)
{
    return controlledByPlayer_[player->GetPlayerId()];
}

