/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SPAWNMASTER_H
#define SPAWNMASTER_H

#include "../mastercontrol.h"

#define SPAWN GetSubsystem<SpawnMaster>()

class SpawnMaster: public Object
{
    friend class MasterControl;
    DRY_OBJECT(SpawnMaster, Object);

public:
    SpawnMaster(Context* context);

    void Clear();

    template <class T> T* Create(bool enabled = false, bool recycle = true)
    {
        T* created{ nullptr };

        if (recycle)
        {
            for (Node* n: created_[T::GetTypeStatic()])
            {
                if (!n->IsEnabled())
                {
                    created = n->GetComponent<T>();
                    break;
                }
            }
        }

        if (!created)
        {
            Node* spawnedNode{ MC->GetScene()->CreateChild(T::GetTypeStatic().ToString()) };
            created = spawnedNode->CreateComponent<T>();

            if (!enabled)
                spawnedNode->SetEnabledRecursive(false);

            if (!created_.Contains(T::GetTypeStatic()))
                created_[T::GetTypeStatic()] = Vector<Node*>();

            created_[T::GetTypeStatic()].Push(spawnedNode);
        }

        return created;
    }

    template <class T> Vector<Node*> GetAll() { return created_[T::GetTypeStatic()]; }

    template <class T> Vector<Node*> GetActive()
    {
        Vector<Node*> nodes{ GetAll<T>() };

        for (Node* n: nodes)
        {
            if (!n->IsEnabled())
                nodes.RemoveSwap(n);
        }

        return nodes;
    }

    template <class T> int CountActive() { return GetActive<T>().Size(); }

private:
    void Activate();
    void Deactivate();
    void Restart();

    void HandleSceneUpdate(StringHash eventType, VariantMap &eventData);

    HashMap<StringHash, Vector<Node*> > created_;
};

#endif // SPAWNMASTER_H

