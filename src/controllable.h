/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CONTROLLABLE_H
#define CONTROLLABLE_H

#include <bitset>
#include "player/player.h"

#include "sceneobject.h"

class Controllable: public SceneObject
{
    friend class InputMaster;
    DRY_OBJECT(Controllable, SceneObject);

public:
    Controllable(Context* context);

    void Start() override;
    void Update(float timeStep) override;

    void SetMove(Vector2 move);
    void SetAim(Vector2 aim);
    virtual void ClearControl();

    virtual void Think(float timeStep) {}

    Vector2 GetLinearVelocity() const { return rigidBody_->GetLinearVelocity(); }

    Player* GetPlayer();
protected:
    bool controlled_;
    Vector2 move_;
    Vector2 aim_;
    float thrust_;
    float maxSpeed_;
    float maxPitch_;
    float minPitch_;

    std::bitset<4> actions_;
    HashMap<int, float> actionSince_;
    HashMap<int, float> actionInterval_;

    Node* graphicsNode_;
    RigidBody2D* rigidBody_;
    CollisionCircle2D* collisionShape_;

    void ResetInput() { move_ = aim_ = Vector2::ZERO; actions_.reset(); }
    void SetActions(std::bitset<4> actions);

    void AlignWithVelocity(float timeStep);
    void AlignWithMovement(float timeStep);

    virtual void HandleAction(int actionId);
};

#endif // CONTROLLABLE_H

