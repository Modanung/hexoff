/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "sceneobject.h"

SceneObject::SceneObject(Context *context): LogicComponent(context),
    randomizer_{ Random() }
{
}

void SceneObject::Set(const Vector3& position)
{
    node_->SetPosition(position);
    node_->SetEnabledRecursive(true);

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(SceneObject, BlinkCheck));
}

void SceneObject::Disable()
{
    node_->SetEnabledRecursive(false);

    UnsubscribeFromEvent(E_POSTRENDERUPDATE);
}

Vector3 SceneObject::GetWorldPosition() const
{
    return node_->GetWorldPosition();
}

void SceneObject::BlinkCheck(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
//    if (MC->IsPaused())
//        return;

    Vector3 position{ node_->GetPosition() };
    float radius{ ARENA_RADIUS };

    if (!MasterControl::InsideHexagon(position, radius + 0.05f))
    {
        const Vector3 newPosition{ position - 2.0f * radius * Vector3{ MC->GetHexant(position) } };
        Blink(newPosition);
    }
}

void SceneObject::Blink(const Vector3& newPosition)
{
//    const Vector3 oldPosition{ node_->GetPosition() };
    node_->SetPosition(newPosition);
}
