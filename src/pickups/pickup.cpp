/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "pickup.h"

Pickup::Pickup(Context* context): SceneObject(context),
    initialPosition_{},
    graphicsNode_{},
    pickupModel_{},
    rigidBody_{},
    collisionShape_{}
{
}

void Pickup::Start()
{
    graphicsNode_ = node_->CreateChild();
    pickupModel_ = graphicsNode_->CreateComponent<StaticModel>();

    rigidBody_ = node_->CreateComponent<RigidBody2D>();
    rigidBody_->SetBodyType(BT_DYNAMIC);
    rigidBody_->SetLinearDamping(.75f);
    rigidBody_->SetFixedRotation(true);

    for (int i{ 0 }; i < 1/*7*/; ++i)
    {
        CollisionCircle2D* collisionShape{ node_->CreateComponent<CollisionCircle2D>() };
        collisionShape->SetRadius(1.15f);
        collisionShape->SetRestitution(.666f);

        if (i == 0)
        {
            collisionShape_ = collisionShape;
            continue;
        }

        collisionShape->SetCenter(2.f * ARENA_RADIUS * LucKey::Rotate(Vector2::UP, i * 60.f));
    }

    rigidBody_->SetMass(.666f);

}

void Pickup::DelayedStart()
{
    Set(initialPosition_);
}

void Pickup::Update(float timeStep)
{
    //Spin
    graphicsNode_->Rotate(Quaternion{ 0.f, 0.f, 100.f * timeStep });

    //Float like a float
//    const float floatFactor{ .25f /*- Min(.5f, .5f * Abs(node_->GetPosition().z_))*/ };
//    graphicsNode_->SetPosition(Vector3::BACK * MC->Sine(floatFreq_, -floatFactor, floatFactor, floatShift_));
}
