/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PICKUP_H
#define PICKUP_H

#include "../sceneobject.h"

class Pickup: public SceneObject
{
    DRY_OBJECT(Pickup, SceneObject);

public:
    Pickup(Context* context);

    void Start() override;
    void DelayedStart() override;

    void Update(float timeStep) override;

protected:
    Vector3 initialPosition_;
    float floatFreq_;
    float floatShift_;

    Node* graphicsNode_;
    StaticModel* pickupModel_;

    RigidBody2D* rigidBody_;
    CollisionCircle2D* collisionShape_;
};

#endif // PICKUP_H
