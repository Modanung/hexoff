/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "destructable.h"

Destructable::Destructable(float initialHealth):
    initialHealth_{ initialHealth },
    health_{ initialHealth },
    lastHitBy_{ 0 },
    tally_{}
{
}

void Destructable::Hit(float damage, int pId)
{
    if (health_ == 0.f)
        return;

    lastHitBy_ = pId;


    damage = Min(health_, damage);

    if (pId != 0)
    {
        if (tally_.Contains(pId))
            tally_[pId] += damage;
        else
            tally_[pId] = damage;
    }

    SetHealth(health_ - damage);
}

void Destructable::SetHealth(const float health)
{
    health_ = health;

    if (health_ == 0.f)
        Destroy();
}

float Destructable::Panic() const
{
    if (initialHealth_ == 0.f)
        return 0.f;
    else
        return Clamp(1.f - health_ / initialHealth_, 0.f, 1.f);
}


