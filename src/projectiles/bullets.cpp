/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../destructable.h"
#include "../player/ship.h"

#include "bullets.h"

BillboardSet* Bullets::bulletBoards_{ nullptr };

Bullets::Bullets(Context* context): LogicComponent(context)
{
}

void Bullets::Init(int colorSet)
{
    colorSet_ = colorSet;

    bulletBoards_ = GetScene()->CreateComponent<BillboardSet>();
    bulletBoards_->SetMaterial(RES(Material, "Materials/Bullet.xml"));
    bulletBoards_->SetFaceCameraMode(FC_DIRECTION);
}

void Bullets::Update(float timeStep)
{
    UpdateTransforms(timeStep);

    for (unsigned b{ 0 }; b < bulletBoards_->GetNumBillboards(); ++b)
    {
        Billboard* board{ bulletBoards_->GetBillboard(b) };
        const Bullet& bullet{ bullets_.At(b) };

        board->position_  = Vector3{ bullet.position_ };
        board->direction_ = Vector3{ bullet.velocity_.Normalized() };
        board->size_ = Vector2{ .1f + 0.23f * bullet.damage_, bullet.Grown() };
        board->enabled_ |= true;
    }

    if (bullets_.Size())
        bulletBoards_->Commit();
}

void Bullets::UpdateTransforms(float timeStep)
{
    for (Bullet& bullet: bullets_)
    {
        const Vector2 p{ bullet.position_ };
        const Vector2 v{ bullet.velocity_ };
        const Vector2 d{ v.Normalized() };
        const float   s{ v.Length() };

        PhysicsWorld2D* physics{ GetScene()->GetComponent<PhysicsWorld2D>() };
        PhysicsRaycastResult2D result{};
        const Vector2 begin{ p - d * (.666f + s * timeStep) * bullet.Grown() };
        const Vector2 end{ p + d * bullet.Grown()};

        physics->RaycastSingle(result, begin, end);

        bullet.age_ += timeStep;
        bullet.position_ += bullet.velocity_ * timeStep;

        if (result.body_)
        {
            Node* hitNode{ result.body_->GetNode() };

            if (Destructable* destructable{ hitNode->GetDerivedComponent<Destructable>() })
            {
                Ship* ship{ static_cast<Ship*>(destructable) };
                if (ship && bullet.age_ * s < 10.f && ship->ColorSet() == colorSet_)
                        continue;

                destructable->Hit(bullet.damage_, 0); //// pID
            }

            Hit(bullet, result.position_, true);
            continue;
        }

        if (!MasterControl::InsideHexagon(bullet.position_, ARENA_RADIUS))
        {
            Hit(bullet, bullet.position_, false);
        }
    }
}

void Bullets::Hit(Bullet& bullet, Vector2 position, bool empty)
{
    bullets_.RemoveSwap(bullet);
    bulletBoards_->SetNumBillboards(bullets_.Size());
}

void Bullets::Add(const Vector2& position, const Vector2& velocity, float damage)
{
    const Bullet bullet{ position, velocity, damage };
    bullets_.Push(bullet);

    bulletBoards_->SetNumBillboards(bullets_.Size());
    Billboard& board{ bulletBoards_->GetBillboards().Back() };
    board.color_ = Lerp(Color::GREEN, Color::YELLOW, .5f);
}
