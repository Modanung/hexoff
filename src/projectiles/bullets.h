/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BULLETS_H
#define BULLETS_H

#include "../mastercontrol.h"

struct Bullet
{
    Bullet(): Bullet(Vector2::ZERO, Vector2::UP, .1f) {}

    Bullet(const Vector2 position, const Vector2 velocity, float damage):
        position_{ position },
        velocity_{ velocity },
        damage_{ damage }
    {}

    Vector2 position_;
    Vector2 velocity_;
    float damage_;
    float age_{};

    float Grown() const { return Clamp(velocity_.Length() * age_ * .5f, .2f, 1.f); }

    bool operator !=(const Bullet& rhs)
    {
        return position_ != rhs.position_ || velocity_ != rhs.velocity_;
    }
};

class Bullets: public LogicComponent
{
    DRY_OBJECT(Bullets, LogicComponent);

public:
    Bullets(Context* context);

    void Init(int colorSet);
    void Update(float timeStep) override;

    void Add(const Vector2& position, const Vector2& velocity, float damage);

private:
    int colorSet_;
    static BillboardSet* bulletBoards_;
    Vector<Bullet> bullets_;
    void UpdateTransforms(float timeStep);
    void Hit(Bullet& bullet, Vector2 position, bool empty);
};

#endif // BULLETS_H
