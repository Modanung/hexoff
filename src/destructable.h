/* heXoff
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DESTRUCTABLE_H
#define DESTRUCTABLE_H

#include "luckey.h"

class Destructable
{

public:
    Destructable(float initialHealth);

    virtual void Hit(float damage, int pId);
    float Panic() const;

protected:
    virtual void Destroy() = 0;

private:
    void SetHealth(const float health);

    const float initialHealth_;
    float health_;

    int lastHitBy_;
    HashMap<int, float> tally_;
};

#endif // DESTRUCTABLE_H
