HEADERS += \
    src/luckey.h \
    src/mastercontrol.h \
    src/sceneobject.h \
    src/masters/spawnmaster.h \
    src/masters/inputmaster.h \
    src/player/player.h \
    src/player/ship.h \
    src/controllable.h \
    src/destructable.h \
    src/projectiles/projectile.h \
    src/projectiles/bullets.h \
    src/pickups/pickup.h \
    src/pickups/apple.h \
    src/pickups/heart.h \
    src/enemies/enemy.h \
    src/enemies/razor.h

SOURCES += \
    src/luckey.cpp \
    src/mastercontrol.cpp \
    src/sceneobject.cpp \
    src/masters/spawnmaster.cpp \
    src/masters/inputmaster.cpp \
    src/player/player.cpp \
    src/controllable.cpp \
    src/destructable.cpp \
    src/player/ship.cpp \
    src/projectiles/projectile.cpp \
    src/projectiles/bullets.cpp \
    src/pickups/pickup.cpp \
    src/pickups/apple.cpp \
    src/pickups/heart.cpp \
    src/enemies/enemy.cpp \
    src/enemies/razor.cpp
